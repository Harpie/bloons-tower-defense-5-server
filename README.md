# BTD5 Server Proof of Concept

This is a proof of concept for creating a BTD5 server in Python.

The server only sends back information about connection to the server during its creation.

The aim of this project is to prove that it's possible to create a server for Flash games using newer technologies.

# Running the Server

1. To connect to a local server, you need to modify the hosts file in your system and redirect the game's domain to localhost.

The BTD5 game requires a server at the address btd5main02.ninjakiwi.com.

Add the following to /etc/hosts:

```
127.0.0.1 btd5main02.ninjakiwi.com
```

2. Start the server with the following command:

```
python3.10 DEMO.py
```

The server is run on localhost. If you need to change the IP, modify this line:

```
serverMenu = Main(host='127.0.0.1', port=5577)
```

3. Launch BTD5
- Launch Ninja Kiwi Archive
- Select the BTD5 game
- Create a new private match

At this point, you should be able to connect to the server and get stuck at the next step "Requesting game creation..."

That's the end. After 5 seconds, the server will shut down.
