from time import sleep
from dataclasses import dataclass
from enum import IntEnum
from abc import ABC, abstractmethod

import socket
import _thread
import os
import traceback



class CommandType(IntEnum):
    
    # recived types
    PLAYER_INIT = 17
    FIND_GAME = 2
    CREATE_GAME = 3
    LOG_OUT_1 = 7
    LOG_OUT_2 = 11
    LOG_OUT_3 = 15
    
    # sended types
    ACK_USER_DATA = 18
    
class CommandIncoming:
    DELIMITER = b','
    def __init__(self, income: bytes):
        data = income.split(self.DELIMITER)
        self.COMMAND = CommandType(int(data.pop(0)))
        self.PLAYER = None
        match self.COMMAND:
            case CommandType.PLAYER_INIT:
                self.PLAYER = Player(int(data[1]), data[2])
            case CommandType.CREATE_GAME: 
                # TODO
                pass

    def __str__(self):
        return ("%s - %s") % (self.COMMAND, self.PLAYER)

        
class CommandResponse:
    
    @staticmethod
    def game_created(ip:bytes, port:int):
        # TODO
        pass
    
    @staticmethod
    def ack_create_game_canceled():
        # TODO
        pass
    
    @staticmethod
    def ack_user_data():
        return b'%i,\n' % CommandType.ACK_USER_DATA

        



@dataclass
class Player:
    id: int
    name: bytes

@dataclass
class Connection:
    player: Player
    i_am_host: bool = False

class Server(ABC):
    """Abstract base class for a Server."""
    
    @abstractmethod
    def _process_commands(self, connection_data:Connection, received_commands: list[CommandIncoming]) -> bool:
        pass
    
    def __handle_new_client(self, connection, client_address):
        """Handle each new client connection."""
        connection_data = None
        server_address = connection.getsockname()
        try:
            print(server_address, client_address, 'CLIENT CONNECTED')
            
            def recvall(connection):
                fragments = []
                while True: 
                    chunk = connection.recv(1024)
                    fragments.append(chunk)
                    if len(chunk) < 1024:
                        break
                return b''.join(fragments)
                
            data = b''
            while True:
                data += recvall(connection)
                print(server_address, client_address, 'RECEIVED DATA {!r}'.format(data))
                
                # Flash auth magic
                if b'<policy-file-request/>' in data:
                    connection.sendall(b'<?xml version="1.0"?>\n<!DOCTYPE cross-domain-policy SYSTEM \n"http://www.adobe.com/xml/dtds/cross-domain-policy.dtd">\n<cross-domain-policy>\n<site-control permitted-cross-domain-policies="master-only"/>\n<allow-access-from domain="*" to-ports="*"/>\n</cross-domain-policy>\n')
                    connection.close()
                    return
                    
                sleep(.05)
                    
                data_parsed = []
                while len(tmp := data.split(b'\n', 1)) > 1:
                    [received_command, data] = tmp
                    data_parsed += [CommandIncoming(received_command)]
                
                (connection_data, to_send) = self._process_commands(connection_data, data_parsed)


                if not connection_data:
                    break
                    
                connection.sendall(to_send)
                print(server_address, client_address, 'SEND', to_send)

                if connection_data.player.id not in self._ACTIVE_CONNECTIONS:
                    self._ACTIVE_CONNECTIONS[connection_data.player.id] = connection

                
        except:
            traceback.print_exc()
            print(server_address, client_address, 'UNKNOWN COMMAND')
            # TODO exception handling
            os._exit(1)
        finally:
            if connection_data:
                self.close(connection_data.player.id) 
            print(server_address, client_address, 'DISCONNECTED')

            

    def __accept_new_clients(self):
        """Accept new clients and start new threads for each one."""
        try:
            while True:
                client, address = self.__server_socket.accept()
                _thread.start_new_thread(self.__handle_new_client,(client, address))
        except:
            # printing stack trace
            traceback.print_exc()
            # TODO exception handling
            os._exit(1)
        finally:
            # TODO Clean up the connection
            print('SERVER STOPPED! last client: ', address)
            # TODO exception handling
            os._exit(1)
            
    def send(self, client_id: int, command: bytes):
        """Send a command to a specific client."""
        self._ACTIVE_CONNECTIONS[client_id].sendall(command)
        
    def close(self, client_id: int):
        """Close the connection with a specific client."""
        self._ACTIVE_CONNECTIONS[client_id].close()
        del self._ACTIVE_CONNECTIONS[client_id]
        
        
    def __del__(self):
        self.__server_socket.close()
        
    def __init__(self, host: str, port: int): #'localhost', socket.gethostbyname(socket.gethostname())
        self.__host = bytes(host, 'utf-8')
        self.__port = port
        self._ACTIVE_CONNECTIONS = {}
        self.__server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__server_socket.bind((host, port))
        self.__server_socket.listen(5)
        print ('SERVER STARTED! %s:%s' % (host, port))
        _thread.start_new_thread(self.__accept_new_clients, ())

        
    def get_address(self) -> bytes:
        """Return the server's address."""
        return self.__host
        
    def get_port(self) -> int:
        """Return the server's port."""
        return self.__port

class Main(Server):
    """Main server class, inheriting from the Server abstract base class."""

    def _process_commands(self, client_user_data, received_commands: list[CommandIncoming]) -> bool:
        """Process the received commands."""
        to_send = b''
        for received_command in received_commands:
            match received_command.COMMAND:
                case CommandType.PLAYER_INIT:
                    client_user_data = Connection(received_command.PLAYER, None)
                    
                    
                    to_send += CommandResponse.ack_user_data()

                case CommandType.CREATE_GAME:
                    print("THE END OF DEMO")
                    sleep(5)
                    os._exit(1)
                    # TODO create new serwer
                    #if created:
                    #    to_send += CommandResponse.game_created(adress, port)
                    #else:
                    #    to_send += CommandResponse.ack_create_game_canceled()

                case CommandType.LOG_OUT_1 | CommandType.LOG_OUT_2 | CommandType.LOG_OUT_3:
                    self.close(client_user_data.player.id)
                    client_user_data = None
                    break
                case _:
                    raise Exception("UNKNOWN COMMAND")
                    
        return (client_user_data, to_send)
        



# Start the server
server_menu = Main(host='127.0.0.1', port=5577) # TODO IP DETECTION

while 1==1:
    sleep(10)
    print('OK')




